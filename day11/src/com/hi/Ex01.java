package com.hi;

public class Ex01 {

	public static void main(String[] args) {
		// 문자열
		String st1=null;
		st1="java";
//		st1=new String("java");
		String st2="java";
		// 문자열의 비교 equals메서드
		System.out.println(st1.equals(st2));
		
		char[] arr1=st2.toCharArray();
		String st3=new String(arr1);
		
		byte[] arr2=st2.getBytes();
		String st4=new String(arr2,1,2);
		System.out.println(st4);
	}

}
