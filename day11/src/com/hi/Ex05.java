package com.hi;

public class Ex05 {

	public static void main(String[] args) {
		// 1 user1 1 2 3\n2 user2 44 55 66\n3 user3 77 88 99
		
		
		
		// 학생성적관리프로그램(ver 0.6.0)
		// String data="---------\n학번~~~\n------------\n";
		// 1.보기 2.입력 3.삭제 (4.수정) 0.종료>3
		// 삭제할 학번>1
		// 1.보기 2.입력 3.삭제 (4.수정) 0.종료>4
		// 수정할 학번>2
		// 국어>11
		// 영어>22
		// 수학>33
		// 1.보기 2.입력 3.삭제 (4.수정) 0.종료>0
		// 이용해주셔서 감사합니다
		java.util.Scanner sc=new java.util.Scanner(System.in);
		System.out.println("학생성적관리프로그램(ver 0.6.0)");
		String data="";
		String input=null;
		while(true){
			System.out.println("1.보기 2.입력 3.삭제 (4.수정) 0.종료>");
			input=sc.nextLine();
			if(input.equals("0")){
				return;
			}
			if(input.equals("1")){
				System.out.println("-----------------------------------------------------------------");
				System.out.println("학번\t│이름\t│국어\t│영어\t│수학");
				System.out.print("-----------------------------------------------------------------");
				System.out.println(data);
			}else if(input.equals("2")){
				System.out.print("학번>");
				data+="\n"+sc.nextLine()+"\t│";
				System.out.print("이름>");
				data+=sc.nextLine()+"\t│";
				System.out.print("국어>");
				data+=sc.nextLine()+"\t│";
				System.out.print("영어>");
				data+=sc.nextLine()+"\t│";
				System.out.print("수학>");
				data+=sc.nextLine();
			}else if(input.equals("3")){
				System.out.print("삭제할 학번>");
				input=sc.nextLine();
				int startIdx=data.indexOf("\n"+input+"\t");
				int endIdx=data.indexOf("\n",startIdx+1);
				String target;
				if(endIdx==-1){
					target=data.substring(startIdx);
				}else{
					target=data.substring(startIdx,endIdx);
				}
				data=data.replace(target, "");
			}else if(input.equals("4")){
				System.out.print("수정할 학번>");
				input=sc.nextLine();
				int startIdx=data.indexOf("\n"+input+"\t");
				int endIdx=data.indexOf("\n",startIdx+1);
				String target;
				if(endIdx==-1){
					target=data.substring(startIdx);
				}else{
					target=data.substring(startIdx,endIdx);
				}
				input="\n"+input+"\t│";
				System.out.print("이름>");
				input+=sc.nextLine()+"\t│";
				System.out.print("국어>");
				input+=sc.nextLine()+"\t│";
				System.out.print("영어>");
				input+=sc.nextLine()+"\t│";
				System.out.print("수학>");
				input+=sc.nextLine();
				
				data=data.replace(target, input);
			}
		}

	}

}














