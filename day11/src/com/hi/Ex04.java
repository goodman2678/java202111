package com.hi;

public class Ex04 {

	public static void main(String[] args) {
		String msg="java web db framwork app";
		String[] arr=msg.split(" ");
		for(int i=0; i<arr.length; i++){
			System.out.println(arr[i]);
		}
		System.out.println("-----------------------------------");
		// 같은지 다른지, 다르면 얼마나 다른지 리턴
		String msg1="abca";
		String msg2="acca";
		System.out.println(msg1.compareTo(msg2));
		System.out.println(func(msg1,msg2));
	}
	
	public static int func(String a, String b){
		if(a.length()==b.length()){
			char[] arr1=a.toCharArray();
			char[] arr2=b.toCharArray();
			for(int i=0; i<a.length(); i++){
				if(arr1[i]-arr2[i]!=0){
					return arr1[i]-arr2[i];
				}
			}
			return 0;
		}else{
			return a.length()-b.length();
		}
	}

}
