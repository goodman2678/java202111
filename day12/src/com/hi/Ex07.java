package com.hi;

public class Ex07 {

	public static void main(String[] args) {
		//
		for(int i=0; i<10; i++){

			System.out.println("before:"+i);
			if(i>4){
//				return;		//해당 메서드의 종료(호출한 측으로 돌아가기,스택 빼내기) - 메서드 어디서나
//				continue;	//해당 반복문의 상단으로 돌아감 - 반복문내
//				break;		//해당 switch문||반복문에서 탈출 - 반복문내,switch문
			}
			System.out.println("after:"+i);
			
		}
		System.out.println("main 종료");
	}
	
	

}
