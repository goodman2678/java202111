package com.hi;

class Lec06{
	public int su=1111;
	public Lec06(){
		super();
		System.out.println("부모생성");
	}
	public void func(){
		System.out.println("부모클래스의 기능");
	}
}

public class Ex06 extends Lec06{
	public int su=2222;
	
	public Ex06(){
		this(1);
		System.out.println("자식생성1");
	}
	
	public Ex06(int a){
		this("");
		System.out.println("자식생성2");
		
	}
	
	public Ex06(String a){
		super();
		System.out.println("자식생성3");
		
	}
	
	
	public void func(){
		System.out.println("자식클래스의 기능"+super.su);
		super.func();
	}
	public static void main(String[] args) {
		Ex06 me = new Ex06();
		me.func();
	}

}








