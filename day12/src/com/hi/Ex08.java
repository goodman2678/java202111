package com.hi;

class Lec08{	// final class 상속의 거부
	
	public final void func2(){}	// 오버라이드의 거부
	
}

public class Ex08 extends Lec08 {
//	public final static int su1=0;
	public final int su2;
	
	public Ex08(int su){
		su2=su;
	}
	
	public static void func(final int a){
		System.out.println(a);
	}
	
//	public void func2(){}

	public static void main(String[] args) {
		Lec08 you=new Lec08();
		you.func2();
		Ex08 me=new Ex08(1111);
		me.func2();
		
		
		
		// final
		final int a;	// 상수형 변수
		a=1111;
//		a=2222;
//		Ex08 me1=new Ex08(1111);
//		System.out.println(me1.su2);
//		Ex08 me2=new Ex08(2222);
//		System.out.println(me2.su2);
		int c=3333;
		func(c);
		func(2222);
	}

}
