package com.hi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Ex04 {

	public static void main(String[] args) {
		// 날짜 format변환
		Date cal=new Date();
		cal.setDate(1);
		java.text.DateFormat df=DateFormat.getDateInstance(DateFormat.FULL);
		String msg=df.format(cal);
		System.out.println(msg);
		System.out.println("---------------------------------------------------");
//		포멧 패턴은 문서의 표를 참조
//		https://docs.oracle.com/javase/8/docs/api/java/text/SimpleDateFormat.html
		java.text.SimpleDateFormat sdf=new SimpleDateFormat("yy-M-dd hh:mm");
		msg=sdf.format(cal);
		System.out.println(msg);
	}

}














