package com.hi;

import java.util.LinkedList;

public class Ex08 {

	public static void main(String[] args) {
		// FIFO (first-in-first-out). 
		java.util.Queue que=new LinkedList();
		que.offer("첫번째");
		que.offer("두번째");
		que.offer("세번째");
		que.offer("네번째");
		
//		while(true){
//			if(que.peek()==null){break;}
//			System.out.println(que.peek());
//			que.poll();
//		}
		while(que.peek()!=null){
			System.out.println(que.poll());
		}
	}

}






