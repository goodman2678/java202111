package com.room802;

public class Ex09 {

	public static void main(String[] args) {
		// 학생성적관리 프로그램(ver 0.0.1)
				// kor=96, eng=69, math=73
				// 합계 평균 학점
				// 학점 평균 90이상 A학점
				// 			80이상 B
				//			70이상 C
				//			60이상 D
				//			60미만 F (재수강하세요)
				// -------------------------------------------------
				//	국어:96		| 영어:69		| 수학:73	
				// -------------------------------------------------
				// 합계:0000
				// 평균:00.00	 (소수둘째짜리까지 출력)
				// -------------------------------------------------
				// 학점:F 
				//		재수강하세요	
		String bar="-------------------------------------------------";
		int kor,eng,math,sum;
		double avg;
		kor=96;
		eng=84;
		math=73;
		sum=kor+eng+math;
		avg=sum*100/3/100.0;
		// String 문자열 자료형
		String msg=bar+"\n국어:"+kor+"\t\t|영어:"+eng+"\t|수학:"+math+"\n";
		msg+=bar+"\n합계:"+sum+"\n평균:"+avg+"\t(소수둘째짜리까지 출력)\n"+bar+"\n학점:";
		int su=(int)avg/10;//평균값에 10의 자리 숫자
		if(su==10 || su==9 ){
			msg+="A";	// msg=msg+"A";
		}else if(su==8){
			msg+="B";	// msg=msg+"B";
		}else if(su==7){
			msg+="C";	// msg=msg+"C";
		}else if(su==6){
			msg+="D";	// msg=msg+"D";
		}else{
			msg+="F";	// msg=msg+"F";
		}
		msg+="학점";
		System.out.println(msg);
	}

}















