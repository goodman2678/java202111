package com.hi;

public class Ex05 {
	
	public static int func01(){
		System.out.println("func01 start");
		func02();
		System.out.println("func01 end");
		return 0;
	}

	public static void main(String[] args) {
		System.out.println("main start");
		func01();
		System.out.println("main end");
		return;
	}

	public static void func02(){
		System.out.println("func02()");
		return;
	}
}
