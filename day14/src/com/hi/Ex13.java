package com.hi;

public class Ex13 {
	int su=1234;

	public static void main(String[] args) {
		// 자바 리플렉션
		Ex13 me=new Ex13();
		System.out.println(me.su);

		String info="com.hi.Ex13";
		try {
			Class cls=Class.forName(info);
			Object obj=cls.newInstance();
			Ex13 me2=(Ex13)obj;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
