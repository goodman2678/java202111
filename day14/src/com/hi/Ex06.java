package com.hi;

public class Ex06 {

	public static void main(String[] args) {
		// 숫자 글자(알파벳,한글) 특수문자
		char ch1='a';
		Character ch2=new Character('A');
		Character ch3=new Character((char)65);
		System.out.println(ch1+"은 유니코드이냐"+Character.isDefined(ch1));
		System.out.println(ch1+"은 글자이냐"+Character.isAlphabetic(ch1));
		System.out.println(ch1+"은 글자이냐"+Character.isLetter(ch1));
		System.out.println(ch1+"은 숫자냐"+Character.isDigit(ch1));
		System.out.println(ch1+"은 띄어쓰기"+Character.isSpace(ch1));
		System.out.println(ch1+"은 "+Character.isWhitespace(ch1));
		System.out.println(ch1+"은 "+Character.isUpperCase(ch1));
		System.out.println(ch1+"은 "+Character.isLowerCase(ch1));

	}

}
