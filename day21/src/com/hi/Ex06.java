package com.hi;

import java.awt.Button;
import java.awt.CheckboxMenuItem;
import java.awt.Frame;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.PopupMenu;

public class Ex06 extends Frame{
	
	public Ex06(){
		setTitle("제목");
		Button btn=new Button("버튼");
		
		
		
		Menu mn2=new Menu("Help");
		
		MenuItem mi1=new MenuItem("item1");
		MenuItem mi2=new MenuItem("item2");
		CheckboxMenuItem mi3=new CheckboxMenuItem("item3",true);
		MenuItem mi4=new MenuItem("하위첫번째");
		Menu mn3=new Menu("하위메뉴");
		mn3.add(mi4);
		Menu mn1=new Menu();
		mn1.setLabel("File");
		mn1.add(mi1);
		mn1.add(mn3);
		mn1.addSeparator();
		mn1.add(mi3);
		
		MenuBar mb=new MenuBar();
		mb.add(mn1);
		mb.add(mn2);
		setMenuBar(mb);
		
		PopupMenu pm = new PopupMenu();
	    MenuItem pm_item1 = new MenuItem("popup1");
	    MenuItem pm_item2 = new MenuItem("popup2");
	    pm.add(pm_item1);
        pm.addSeparator(); // 구분선
        pm.add(pm_item2);
	    add(pm);
		
		setBounds(100-1920,100,600,500);
		setVisible(true);
		
		  
		pm.show(this, 300, 300);
	}

	public static void main(String[] args) {
		new Ex06();

	}

}
