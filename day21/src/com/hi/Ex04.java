package com.hi;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.ImageObserver;

public class Ex04 extends Frame{
	
	public Ex04(){
		

		Toolkit tool=Toolkit.getDefaultToolkit();
		final Image img=tool.createImage("img01.png");
		
		java.awt.Canvas can=new Canvas(){
			@Override
			public void paint(Graphics g){
				
				String msg="java awt";
//				g.drawImage(img, 50,50,100,100,this);
				g.drawChars(msg.toCharArray(), 0, msg.length(), 100, 100);
//				g.setColor(Color.RED);
				g.drawLine(100, 100, 200, 200);
//				g.drawRect(200, 200, 50, 50);
//				g.setColor(Color.BLUE);
//				g.drawOval(300, 100, 200, 200);
//				g.drawArc(300, 100, 200, 200, 0, 180);
//				g.fillRect(100, 100, 200, 200);
//				g.setColor(Color.RED);
//				g.fillOval(200, 100, 200, 200);
				g.draw3DRect(100, 100, 200, 200,true);
			}
		};
		
		add(can);
		setLocation(100-1920,100);
		setSize(600,500);
		setVisible(true);
	}

	public static void main(String[] args) {
		new Ex04();
	}

}
