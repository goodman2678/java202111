package com.hi;

import java.io.IOException;

public class Ex01 {

	public static void main(String[] args) {
		// io 바이트스트림(1byte)
		java.io.OutputStream out=System.out;
		try {
			out.write(97);
			out.write(65);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
