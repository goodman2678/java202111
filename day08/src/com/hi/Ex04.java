package com.hi;

public class Ex04 {
//	public final int su;
	public int su3;
	public static final int su5=5555;
//	public static final int su6;
	
	public Ex04(){
		su3=3333;
	}

	public static void main(String[] args) {
		final int su4;	// 상수형 변수
		su4=4444;
//		su4=5555;
		System.out.println(su4);
		
		Ex04 me=new Ex04();
//		System.out.println(me.su);
		System.out.println(me.su3);
//		me.su3=54321;
//		su5=6666;
	}

}
