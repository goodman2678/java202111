package com.hi;

interface Inter{
	void func02();
}

class Lec06{
	public void func01(){}
	public void func02(){}
}

public class Ex06 {
	
	public void func(){
		System.out.println(this);
		Lec06 inter=new Lec06(){
			public void func02(){
				System.out.println("익명클래스 기능"+this);
			}
		};
		inter.func02();
	}
	

	public static void main(String[] args) {
		Ex06 me =new Ex06();
		me.func();
	}

}










