package com.hi;

class Outter05{
	public static int su1=1111;
	public int su2=2222;
	
	public Outter05(){}
	
	public static void func01(final int su5){
			// jdk 1.8 
		class Inner05{
			public final static int su3=3333;
			public int su4=4444;
			public Inner05(){}
			public void func03(){
				System.out.println(su5);
			}
			
		}
//		System.out.println(Inner05.su3);
		Inner05 inn=new Inner05();
		inn.func03();
		
	}
}

public class Ex05 {

	public static void main(String[] args) {
		Outter05.func01(6666);
	}

}
